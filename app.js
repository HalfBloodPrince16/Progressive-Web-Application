const apiKey = 'b652b82b8eb346868b0b42643c2bd877';
const main = document.querySelector('main');

window.addEventListener('load',e=>{
  updateNews();
});

async function updateNews(){
  const res = await fetch('https://newsapi.org/v2/top-headlines?' +'sources=bbc-news&' + 'apiKey=b652b82b8eb346868b0b42643c2bd877')
  const json = await res.json();

  main.innerHTML = json.articles.map(createArticle).join('\n');

}
function createArticles(article){
  return'<div class ="article">
    <a href="${article.url}">
      <h2>${article.title}</h2>
      <img src="${article.urlToImage}">
      <p>${article.description}</p>
    </a>
  </div>';
}
